(include "reflection.lib")
(include "random.lib")

(define (author)
	(println "AUTHOR: Melissa Jenkins mjjenkins1@crimson.ua.edu")
	)

;************************************************************************************************
;Problem 1
;************************************************************************************************
(define (run1)
	(inspect (check-and-go 1 nil))
	(println "	[it should be 1]")
	)

(define (check-and-go $f g)
	(if (error? (catch (eval $f this)))
		(g (catch (eval $f this)))
		(eval $f this)
		)
	)

;***********************************************************************************************
;Problem 2
;***********************************************************************************************
(define (run2)
	(inspect (define (f a b c) (+ a b c)))
	(inspect ((((curry f) 1) 2) 3))
	(println "	[it should be 6]")
	)

(define (buildLambdaList currentList params)
	(cond 
		((null? params) currentList)
		(else (buildLambdaList (list 'lambda (list (car params)) currentList) (cdr params))
			)
		)
	)

(define (curry f)
	(eval (buildLambdaList (cons (get 'name f) (get 'parameters f)) 
				(reverse (get 'parameters f))) this)
	)

;************************************************************************************************
;Problem 3
;************************************************************************************************
(define (run3)
	(inspect (translate zero))
	(println "	[it should be zero]]")
	(inspect (increment zero))
	(println "	[it should be an anonymous incrementor function]")
	(inspect (add one two))
	(println "	[it should be an anonymous incrementor function]")
	(println)
	)

(define (identity x) x)

(define (increment number)
	(lambda (incrementor)
		(define (resolver base)
			(incrementor ((number incrementor) base))
			)
		resolver
		)
	)

(define (inc x) (cons 'x x))
(define base '(x))

;Number defitions
(define zero (lambda (inc) (lambda (x) x)))
(define one (increment zero))
(define two (increment one))
(define three (increment two))
(define four (increment three))
(define five (increment four))
(define six (increment five))
(define seven (increment six))
(define eight (increment seven))
(define nine (increment eight))

;Define list of numbers
(define numbers
  (list
	(list ((zero inc) base) 'zero)
	(list ((one inc) base) 'one)
	(list ((two inc) base) 'two)
	(list ((three inc) base) 'three)
	(list ((four inc) base) 'four)
	(list ((five inc) base) 'five)
	(list ((six inc) base) 'six)
	(list ((seven inc) base) 'seven)
	(list ((eight inc) base) 'eight)
	(list ((nine inc) base) 'nine)
	)
  )

(define (add num1 num2)
  (lambda (inc)
	(lambda (x)
	  ((num1 inc) ((num2 inc) x))
	  )
	)
  )

(define (translate number)
  (cadr (assoc ((number inc) base) numbers))
  )

;**********************************************************************************************
;Problem 4
;**********************************************************************************************
(define (run4)
	(inspect (nodeCount (node 0)))
	(println "	[it should be 1]")
	)

(define (node value @)
	(cond 
		((null? @) (list value))
		(else (cons value (cons (car @) (cadr @))))
		)
	)

(define (isAtom? node)
	(equal? (cdr node) nil)
	)

(define (nodeCount items)
	(cond
		((null? items) 0)
		((isAtom? items) 1)
		(else (+ 1 (nodeCount (cadr items)) (nodeCount (cddr items))))
		)
	)

;*********************************************************************************************
;Problem 5
;*********************************************************************************************
(define (run5)
	(inspect (dot-product '(1 2) '(3 4)))
	(println "	[it should be 11]")
	(inspect (matrix-*-vector '((1 2) (3 4)) '(5 6)))
	(println "	[it should be (23 34)]")
	(inspect (transpose '((1 2) (3 4))))
	(println "	[it should be ((1 3) (2 4))]")
	(inspect (matrix-*-matrix '((1 2) (3 4)) '((1 0) (0 1))))
	(println "	[it should be ((1 3) (2 4))]")
	)

(define (accumulate operator initial sequence)
	(if (null? sequence)
		initial
		(operator (car sequence)
			(accumulate operator initial (cdr sequence)))))

(define (accumulate-n operator initial sequence)
	(if (null? (car sequence))
		nil
		(cons (accumulate operator initial (map car sequence))
			(accumulate-n operator initial (map cdr sequence)))
		)
	)

(define (dot-product v w)
	(accumulate + 0 (map * v w)
		)
	)

(define (matrix-*-vector m v)
	(map (lambda (mCol)
		(dot-product mCol v)) (transpose m)
		)
	)

(define (transpose m)
	(accumulate-n cons '() m)
	)

(define (matrix-*-matrix m n)
	(let ((cols (transpose n)))
		(map (lambda (x) (matrix-*-vector cols x)) (transpose m))
		)
	)

;******************************************************************************************
;Problem 6
;******************************************************************************************

(define (run6)
	(inspect (tuple 1 2))
	(println "	[it should be ((0) (1) (2))]")
	)

(define (flatten items)
	(cond
		((null? items) '())
		((not (pair? items)) (list items))
		(else
			(append
				(flatten (car items)) (flatten (cdr items))
				)
			)
		)
	)

(define (pairs n)
	(define (helper count)
		(cond
			((> count n) nil)
			(else
				(append 
					(map (lambda (x) (flatten (list count x))) (enumerate-interval 0 n))
					(helper (+ count 1))
					)
					
				)
			)
		)
	(helper 0)
	)

(define (enumerate-tuples size limit)
	(define (helper count)
		(cond
			((= size 2) (pairs limit))
			(else
				(cond
					((> count limit) nil)
					(else
						(append
							(map (lambda (x) (flatten (list count x))) (enumerate-tuples (- size 1) limit))
							(helper (+ count 1))
							)
						)
					)
				)
			)
		)
	(helper 0)
	) 

(define (isAscending? items)
	(cond
		((null? items) #t)
		((not (pair? items)) items)
		((null? (cdr items)) #t)
		(else
			(and (<= (car items) (car (cdr items))) (isAscending? (cdr items)))
			)
		)
	)

(define (tuple size limit)
	(cond
		((= size 0) '())
		((= size 1) (enumerate-interval 0 limit))
		(else
			(filter isAscending? (enumerate-tuples size limit))
			)
		)
	)

(define (enumerate-interval low high)
	(cond
		((> low high) nil)
		(else
			(cons (list low) (enumerate-interval (+ low 1) high)))
		)
	)

(define (filter p? items)
	(cond
		((null? items) '())
		((p? (car items)) (cons (car items) (filter p? (cdr items))))
		(else (filter p? (cdr items)))
		)
	)

(define (map f items)
	(cond
		((null? items) nil)
		(else (cons (f (car items)) (map f (cdr items))))
		)
	)

;******************************************************************************************
;Problem 7
;******************************************************************************************
(define (run7)
	(inspect (infix->prefix '(3 ^ 2 / 4)))
	(println "	[it should be (^ 3 (/ 2 4))]")
	)

(define (infix->prefix expr)
  (inspect (fixExponentExpr expr))
  (inspect (fixOperatorExpr "/" (fixExponentExpr expr)))
  )

(define (findNextOperator prevItem items)
;(ppTable prevItem)
  (cond
	((null? items) '())
	((equal? (car items) "^") (append (fixExponentExpr prevItem items) (findNextOperator (cadr items) (cddr items))))
	(else
	  (println (car items))
	  (list (car items) (findNextOperator (car items) (cdr items))))
	)
  )

(define (fixExponentExpr items)
  (define (expHelper prev curr)
	(cond
	  ((null? curr) prev)
	  ((equal? (string (car curr)) "^")
	   (list (car curr) prev (expHelper curr (cdr curr))))
	  (else
		 (expHelper (car curr) (cdr curr)))
	  )
	)
  (cond
	((null? items) '())
	(else
	  (expHelper '() items))
	)
  )
  
(define (fixOperatorExpr operator items)
  (define (opHelper prev curr)
	(cond
	  ((null? curr) prev)
	  ((equal? (string (car curr)) operator)
	   (list (car curr) prev (opHelper curr (cdr curr))))
	  (else
		(opHelper (car curr) (cdr curr)))
	  )
	)
  (cond
	((null? items) '())
	(else
	  (opHelper '() items))
	)
  )
	
;*******************************************************************************************
;Problem 8
;*******************************************************************************************
(define (run8)
  (inspect (cxr 'da))
	(inspect ((cxr 'add) '(1 2 3 4 5 6)))
	)

(define (cxr str)
  (define (helper lambdaList curr)
	(inspect lambdaList)
	(cond
		((equal? curr nil) '())
		((equal? (string curr) "a")
		 (list lambdaList '(car x)))
		((equal? (string curr) "d")
		 (list lambdaList '(cdr x)))
		((equal? (car (string curr)) "a")
		 (helper (list 'car lambdaList ) (cdr (string curr))))
		(else
		  (helper (list 'cdr lambdaList) (cdr (string curr))))
		)
	)
  (cond
	((equal? str nil) '())
	((equal? (string str) "a")
		(lambda (x) (car x)))
	((equal? (string str) "d")
	 	(lambda (x) (cdr x)))
	((equal? (car (string str)) "a")
	 (eval (list 'lambda (list 'x) (helper 'car (cdr (string str)))) this))
	(else
	  (eval (list 'lambda (list 'x) (helper 'cdr (cdr (string str)))) this))
	)

	;(lambda (x) (helper '() str))
	)
	




(define (cxrHelper curr str)
	(inspect curr)
	(inspect str)
	(cond
		((equal? str  nil) curr)
		((equal? (car (string str)) "") curr)
		(else
			(cond
				((equal? (car (string str)) "a")
					 (cxrHelper (list 'car curr) (cdr (string str))))
				(else
					(cxrHelper (list 'cdr curr) (cdr (string str))))
				)
			)
		)
	)

;******************************************************************************************
;Problem 9
;******************************************************************************************

(define (run9)
	(define (- arg1 arg2)
		(cond
			((and (string? arg1) (string? arg2)) 
				(diff arg1 arg2))
			(else
				(+ arg1 (* -1 arg2))
				)
			)
		)
	(inspect (word2int "love"))
	(println "	[it should be 54]")
	(inspect (word2int (- "love" "hate")))
	(println "	[it should be 20]")
	(inspect (word2int (diff "love" "hate")))
	(println "	[it should be 20]")
	(inspect (- 5 3))
	(println "	[it should be 2]")
	)

(define (diff str1 str2)
  (define num1 (word2int str1))
  (define num2 (word2int str2))
  (define difference (abs (- num1 num2)))
  
  (cond
	((= 0 difference) "there is no difference")
	(else
		(define wordsWithDiffValue (getWordsWithValue difference (fileToList)))
		(cond
			((null? wordsWithDiffValue) "the difference is unknown to us")
			(else
				(randomSeed (integer (time)))
				(getElement wordsWithDiffValue (remainder (randomInt) (length wordsWithDiffValue)))
				)
			)
		)
	  )
	)

(define (fileToList)
  (define p (open "words" 'read)) ;p points to port
  (define oldInput (setPort p))
  ;Read Stuff
  (define (helper strList curr)
	(cond
	  ((eof?) 
	   (close p)
	   (setPort oldInput)
	   strList)
	   (else
		;(inspect (define nextStr (readLine)))
		(helper (cons curr strList) (readLine))
		)
	  )
	)
   (helper '() (readLine))
  )

(define (getWordsWithValue value listOfWords)
	(define (helper wordList curr)
		(cond 
			((null? curr) wordList)
			((equal? (word2int (car curr)) value) 
				(helper (cons (car curr) wordList) (cdr curr))
				)
			(else
				(helper wordList (cdr curr))
				)
			)
		)
	(helper '() listOfWords)
	)
(define (word2int str)
  (define (helper total curr)
	(cond
	  ((null? curr) total)
	  ((equal? "" curr) total)
	  (else
		(helper (+ total (- (ascii (car curr)) 96)) (cdr curr))
		)
	  )
	)
  (helper 0 str)
  )
;*******************************************************************************************
;Problem 10
;*******************************************************************************************
(define (run10)
;{	(inspect (((Integer 0) 'toString)))
	(println "	[it should be 0]\n")
	(inspect (((Integer 0) 'rank)))
	(println "	[it should be a number]\n")
	(inspect (((+ (Integer 0) (Integer 0) (Integer 0)) 'value)))
	(println "	[it should be 0]\n")
	(inspect (+ 3 5 1))
	(println "	[it should be 9]\n")
;}
	(println ((Rational 10 50)))
	;(inspect ((Rational 10 50) 'toString))
	;(inspect ((add-rational (Rational 10 50) (Rational 2 7)) 'toString))
	)
;{
(define (gcd a b)
  (if (= b 0)
	a
	(gcd b (remainder a b))
	)
  )

;Integer
;Rational
(define (Rational n d)
	
	(define (numer)
	  (let ((g (gcd n d))))
	  (/ numer g)
	  )
	
	(define (denom)
	  (let ((g (gcd n d))))
	  (/  denom g)
	  )

	(define (toString)
	  (string+ ((car this)) " / " ((cdr this)))
	  ;(display ((car this)))
	  ;(display "/")
	  ;(display ((cdr this)))
	  )

	(define (display item)
	  (if (and (is? item 'environment) (local? 'toString item))
		(__display ((item 'toString)))
		(__display item)
		)
	  )

	(define print (clone print))
	(define println (clone println))

	(cons numer denom)
	this
	)

(define (add-rational x y)
  (Rational (+ (* (x 'numer) (y 'denom))
			   (* (y 'numer) (x 'denom)))
			(* (x 'denom) (y 'denom))
			)
  )

;Real
;Complex Object
(define (Complex real imaginary)
  	(list real imaginary)
	)

(define (real complexObject)
  	(car complexObject)
	)

(define (imaginary complexObject)
  	(cadr complexObject)
	)
;}
;********************************************************************************************
;Run Calls
;********************************************************************************************
(author)
;(run1) ;DONE
;(run2) ;DONE
;(run3) ;DONE I THINK, DO I NEED TO INCLUDE EXPLANATION? AND IS ADD OKAY?
;(run4) ;DONE
;(run5) ;DONE
;(run6) ;DONE
;(run7)
(run8)
;(run9) ;DONE
;(run10)
